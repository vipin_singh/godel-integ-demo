Godel Integration Demo
----------------------

This project demonstrates how to integrate your project with Godel library to make payment ultra easy for your customers. The project is based on Gradle and hence the dependencies are managed using the build.gradle file. 

#### **Add Godel library to your project**
Godel library is made available as a maven dependency and can be directly added to the build script of the project. In addition to the library itself, two more project are required: **Android support library** and **Android AppCompat library**. The following should suffice:

```
repositories {
    mavenCentral()
    maven {
        url "https://s3-ap-southeast-1.amazonaws.com/godel-release/godel/"
    }
}

dependencies {
    compile 'com.android.support:support-v4:+'
    compile 'in.juspay:godel:0.4rc3'
}
```

#### **Authentication URL**
Godel's core logic is situated in a Fragment called ```JuspayBrowserFragment```. This fragment needs to be initialized with a **url** which has to be an HTTP GET URL. Typically, you would provide the payment options in a native screen and thereafter open a webview with a URL for redirecting the user to the bank through the PG provider. Another scenario is when a webview is used for even providing the payment options to the customer. In both the scenarios, the first URL that you load in the ```WebView``` qualifies as the input to the ```JuspayBrowserFragment```.

```
JuspayBrowserFragment juspayBrowserFragment = new JuspayBrowserFragment();
Bundle args = new Bundle();
args.putString("url", "https://merchant.shop.com/cart/payment");
juspayBrowserFragment.setArguments(args);
```
#### **Activating the Fragment**
Once the Fragment is initialized, you can activate it using the ```FragmentManager``` of Android. 
```
FragmentTransaction transaction = this.getSupportFragmentManager().beginTransaction();
transaction.replace(R.id.container, juspayBrowserFragment);
transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
transaction.commit();
```
JuspayBrowserFragment brings its own WebView. So, you don't have to set anything more here, except for activating the Fragment in a suitable place. The Fragment takes up all the space that is provided for it. Usually, the only components in these screens are ActionBar and JuspayBrowserFragment.

```
<FrameLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:id="@+id/container"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:ignore="MergeRootFrame" />
```
#### **Extending the JuspayWebViewClient**
WebViewClient exposes several critical methods like ```onPageStarted```, ```onPageFinished```, ```shouldOverrideUrlLoading```, etc.. These methods are crucial to know when payment flow is complete. JuspayBrowserFragment has made provision for overriding the WebViewClient that is set on the WebView. 
```
public class CustomWebViewClient extends JuspayWebViewClient {
    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
    }    
}
```
**Note:** For all the overridden methods, please remember to call the parent method as well. This is very essential for the library to work properly. 

After extending JuspayWebViewClient, it is time to setup this class in the JuspayBrowserFragment. To accomplish this, JuspayBrowserFragment itself needs to be extended, but inline. The following code will accomplish this:

```
JuspayBrowserFragment browserFragment = new JuspayBrowserFragment(){
    @Override
    protected JuspayWebViewClient newWebViewClient(WebView webView) {
        CustomWebViewClient customWebViewClient = new CustomWebViewClient(webView, this, RechargeActivity.this);
        return customWebViewClient;
    }
};
```
Define your methods in the CustomWebViewClient class and handle the condition where you think the payment flow is complete and the control has to be taken back to the application.
```
@Override
public void onPageFinished(WebView view, String url) {
    if(view.getTitle() != null && view.getTitle().equals("Juspay Payment Response")) {
        rechargeActivity.handlePaymentResponse(url);
    }
    else {
        super.onPageFinished(view, url);
    }
}

```
#### **Manifest changes**
The activity which is holds the JuspayBrowserFragment will have to have these attributes set on them in the manifest file. 
```
<activity
        android:name="com.example.PaymentActivity"
        android:screenOrientation="portrait"
        android:windowSoftInputMode="adjustResize"
        android:label="@string/title_authenticate_payment" >
</activity>
```

#### **Analytics**
Plenty of information from the user's session is logged to our servers for analytics purposes. However, the payment attributes itself will be available to us only if they are set appropriately. 

***PaymentDetails***
PaymentDetails should be set at the time of creation of the Fragment itself. Attributes like merchantId and clientId are required. If they are not sent, then they won't show up in the analytics dashboard. The following attributes can be sent to us so that we can include them in our analytics:

```
import in.juspay.godel.core.PaymentDetails;

// setup the initial parameters for the browser fragment
PaymentDetails paymentDetails = new PaymentDetails();
paymentDetails.setOrderId(rechargeRequest.getPaymentId());
paymentDetails.setMerchantId("juspay_recharge");
// clientId uniquely identifies the weblab configuration rules to apply
paymentDetails.setClientId("juspay_recharge_android");
// customerId uniquely identifies a customer
paymentDetails.setCustomerId(rechargeRequest.getMobileNumber());
paymentDetails.setDisplayNote(String.format("Recharging %s for Rs. %s", rechargeRequest.getMobileNumber(),
        rechargeRequest.getAmount()));
paymentDetails.setRemarks(String.format("Recharging %s for Rs. %s", rechargeRequest.getMobileNumber(),
        rechargeRequest.getAmount()));
paymentDetails.setAmount(rechargeRequest.getAmount());
paymentDetails.setCustomerEmail("");
paymentDetails.setCustomerPhoneNumber(rechargeRequest.getMobileNumber());
Map<String, String> extraParams = new HashMap<String, String>();
// include any extra parameters that needs tracking
extraParams.put("operator", rechargeRequest.getOperator());
extraParams.put("circle", rechargeRequest.getCircle());
extraParams.put("type", "prepaid");

paymentDetails.setExtraParams(extraParams);
// finally set all the information on browser fragment.
browserFragment.setPaymentDetails(paymentDetails);
```
#### ***Payment Status***
As soon as the payment is completed, the control is delegated to your Activity. At this point, however, our library would not know if the payment was successful or not. Hence, we would require you to provide us with this information. Payment status can be sent to us using the following function call:

```
GodelTracker.getInstance().trackPaymentStatus(rechargeRequest.getPaymentId(), PaymentStatus.SUCCESS);
```

Payment status can be one of the three values: SUCCESS, FAILURE, CANCELLED.

#### **Support**
For any queries or clarifications, please do reach out to us at the following email address: support@juspay.in.