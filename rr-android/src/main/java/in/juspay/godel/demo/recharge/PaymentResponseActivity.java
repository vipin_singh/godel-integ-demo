package in.juspay.godel.demo.recharge;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.IntentCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import in.juspay.godel.analytics.GodelTracker;
import in.juspay.godel.core.PaymentResponse;
import in.juspay.godel.demo.R;
import in.juspay.godel.ui.GodelFragment;
import in.juspay.godel.ui.NetworkTask;

import java.net.URL;

import static in.juspay.godel.analytics.GodelTracker.PaymentStatus;

public class PaymentResponseActivity extends FragmentActivity {

    private static final String LOG_TAG = PaymentResponseActivity.class.getName();
    PaymentResponse paymentResponse;
    RechargeRequest rechargeRequest;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = super.getIntent();
        String url = intent.getStringExtra("url");
        paymentResponse = (PaymentResponse) intent.getSerializableExtra("paymentResponse");
        rechargeRequest = (RechargeRequest) intent.getSerializableExtra("rechargeRequest");

        setContentView(R.layout.activity_recharge_screen);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new RechargeResponseFragment(paymentResponse))
                    .commit();
        }
    }

    public class RechargeResponseFragment extends GodelFragment {

        TextView mPaymentStatus;
        TextView mRechargeStatus;
        TextView mMobileNumber;
        TextView mAmount;
        TextView mHelpText;
        Button mNextAction;

        PaymentResponse paymentResponse;

        public RechargeResponseFragment(PaymentResponse paymentResponse) {
            this.paymentResponse = paymentResponse;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_recharge_response, container, false);
            mPaymentStatus = (TextView) rootView.findViewById(R.id.rch_res_payment_status);
            mRechargeStatus = (TextView) rootView.findViewById(R.id.rch_res_recharge_status);
            mMobileNumber = (TextView) rootView.findViewById(R.id.rch_res_mobile_number);
            mAmount = (TextView) rootView.findViewById(R.id.rch_res_amount);
            mHelpText = (TextView) rootView.findViewById(R.id.rch_res_help_text);
            mNextAction = (Button) rootView.findViewById(R.id.rch_res_next_action);

            mMobileNumber.setText(rechargeRequest.getMobileNumber());
            mAmount.setText("INR " + rechargeRequest.getAmount());
            // Send the transactionId and the transaction status to Godel
            GodelTracker.getInstance().trackPaymentStatus(rechargeRequest.getPaymentId(), getPaymentStatus(paymentResponse));
            if(paymentResponse.getStatus().equals("CHARGED")) {
                mPaymentStatus.setText("SUCCESS");
                // initialization
                new NetworkTask<URL, String, String>(PaymentResponseActivity.this){
                    @Override
                    protected String doInBackground(URL... urls) {
                        if(paymentResponse.getStatus().equals("CHARGED")) {
                            try {
                                rechargeRequest = new RechargeService().makeRecharge(rechargeRequest);
                            }
                            catch(Exception e) {
                                Log.e(LOG_TAG, "Exception while invoking make recharge: ", e);
                            }

                        }
                        return "done";
                    }

                    @Override
                    protected void onPostExecute(String s) {
                        super.onPostExecute(s);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(rechargeRequest.getRechargeStatus() == null) {
                                    rechargeRequest.setRechargeStatus("FAILURE");
                                }
                                mRechargeStatus.setText(rechargeRequest.getRechargeStatus());
                                if(rechargeRequest.getRechargeStatus().equals("FAILURE")) {
                                    mHelpText.setText(Html.fromHtml("Sorry! Your recharge has failed." +
                                    " Your payment has been reversed and amount will be refunded in 2 working days." +
                                    " The reference for this transaction is <b>#" + rechargeRequest.getId() + "</b>." +
                                    " For any queries, please write to us at this address: support@rocketrecharge.com."));
                                    handleFailure(mNextAction);
                                }
                                else {

                                    mHelpText.setText(Html.fromHtml("Congratulations! Your recharge has been processed successfully." +
                                    " The reference for this recharge is <b>#" + rechargeRequest.getId() + "</b>." +
                                    " For any queries, please write to us at this address: support@rocketrecharge.com."));
                                    handleSuccess(mNextAction);
                                }
                            }
                        });
                    }
                }.execute(null, null, null);
            }
            else {
                mPaymentStatus.setText("FAILURE");
                mRechargeStatus.setText("FAILURE");
                mHelpText.setText("Sorry. Your payment has failed. Please restart your recharge.");
                handleFailure(mNextAction);
            }

            // return the root view
            return rootView;
        }

        private PaymentStatus getPaymentStatus(PaymentResponse paymentResponse) {
            return paymentResponse.getStatus().equals("CHARGED") ? PaymentStatus.SUCCESS : PaymentStatus.FAILURE;
        }

        private void handleSuccess(final Button mNextAction) {
            mNextAction.setVisibility(View.VISIBLE);
            mNextAction.setText("Do Another Recharge");
            mNextAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mNextAction.setText("Processing...");
                    mNextAction.setEnabled(false);
                    Intent intent = new Intent(getActivity(), RechargeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });

        }

        private void handleFailure(final Button mNextAction) {
            mNextAction.setVisibility(View.VISIBLE);
            mNextAction.setText("Try Again");
            mNextAction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mNextAction.setText("Processing...");
                    mNextAction.setEnabled(false);
                    Intent intent = new Intent(getActivity(), RechargeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP
                            | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
        }
    }

}
