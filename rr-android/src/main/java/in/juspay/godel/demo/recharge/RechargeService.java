package in.juspay.godel.demo.recharge;

import android.util.Log;
import in.juspay.godel.core.PaymentResponse;
import in.juspay.godel.core.RestClient;
import in.juspay.godel.demo.model.PaymentAuthenticationResponse;
import in.juspay.godel.demo.model.PaymentRequest;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RechargeService {

    private static final String LOG_TAG = RechargeService.class.getName();

    public RechargeRequest createRechargeRequest(String mobileNumber, Integer amount) {
        RechargeRequest rechargeRequest = new RechargeRequest();
        rechargeRequest.setAmount(amount.toString());
        rechargeRequest.setEmail("user@email.com");
        rechargeRequest.setMobileNumber(mobileNumber);
        return createRechargeRequest(rechargeRequest);
    }

    private String nullSafeGet(String value) {
        return value == null ? "" : value;
    }

    public RechargeRequest createRechargeRequest(RechargeRequest rechargeRequest) {
        // convert the rechargeReques to JSON
        Map<String, Object> parameters = new HashMap<String, Object>();
        try {
            parameters.put("type", "PREPAID_RECHARGE");
            parameters.put("mobileNumber", rechargeRequest.getMobileNumber());
            parameters.put("email", nullSafeGet(rechargeRequest.getEmail()));
            parameters.put("operator", nullSafeGet(rechargeRequest.getOperatorCode()));
            parameters.put("circle", nullSafeGet(rechargeRequest.getCircleCode()));
            parameters.put("amount", rechargeRequest.getAmount());
            parameters.put("clientId", "Android");

            Log.d(LOG_TAG, "https://www.rocketrecharge.com/recharges/create");
            String jsonResponse = RestClient.postData("https://www.rocketrecharge.com/recharges/create", parameters);
            Log.d(LOG_TAG, "JSON Response is " + jsonResponse);
            JSONObject jsonObj = new JSONObject(jsonResponse);
            JSONObject paymentNode = (JSONObject) jsonObj.get("payment");
            String id = (String) jsonObj.get("id");
            rechargeRequest.setId(id);
            Log.d(LOG_TAG, "Recharge Id is: " + id);
            String paymentId = (String) paymentNode.get("id");
            rechargeRequest.setPaymentId(paymentId);
        } catch(Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Error while creating JSON object " + e.getMessage());
        }
        return rechargeRequest;
    }

    public PaymentAuthenticationResponse startPayment(PaymentRequest paymentRequest) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        try {
            parameters.put("orderId", paymentRequest.getOrderId());
            parameters.put("merchantId", paymentRequest.getMerchantId());
            parameters.put("cardNumber", paymentRequest.getCard().getCardNumber());
            parameters.put("cardExpYear", paymentRequest.getCard().getExpiryYear());
            parameters.put("cardExpMonth", paymentRequest.getCard().getExpiryMonth());
            parameters.put("cardSecurityCode", paymentRequest.getCard().getCVV());
            parameters.put("name", paymentRequest.getCard().getNameOnCard());

            Log.d(LOG_TAG, "Payment Requset is " + paymentRequest.toString());
            Log.d(LOG_TAG, "POST payload: " + serialize(parameters));

            String jsonResponse = RestClient.postData("https://api.juspay.in/payment/handlePay", parameters);
            Log.d(LOG_TAG, "JSON Response is " + jsonResponse);

            JSONObject jsonObj = new JSONObject(jsonResponse);
            String authenticationUrl = (String) jsonObj.get("vbv_url");
            PaymentAuthenticationResponse authResponse = new PaymentAuthenticationResponse();
            authResponse.setAuthenticationUrl(authenticationUrl);
            return authResponse;
        } catch (Throwable e) {
            e.printStackTrace();
            throw new RuntimeException("Error while creating JSON object " + e.getMessage());
        }
    }

    public RechargeRequest makeRecharge(RechargeRequest rechargeRequest) {
        Map<String, Object> parameters = new HashMap<String, Object>();
        try {
            Log.d(LOG_TAG, "MakeRecharge invoking for: " + rechargeRequest.getId());
            parameters.put("rechargeRequestId", rechargeRequest.getId());
            String url = "https://www.rocketrecharge.com/recharge/makeRecharge";
            String jsonResponse = RestClient.postData(url, parameters);
            Log.d(LOG_TAG, jsonResponse);
            JSONObject jsonObject = new JSONObject(jsonResponse);
            JSONObject rechargeStatusObject = jsonObject.getJSONObject("rechargeResponse");
            rechargeRequest.setRechargeStatus(rechargeStatusObject.getString("status"));
        } catch(Exception e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return rechargeRequest;
    }

    public PaymentResponse checkPaymentStatus(RechargeRequest rechargeRequest) {
        Map<String, String> parameters = new HashMap<String, String>();
        parameters.put("order_id", rechargeRequest.getPaymentId());
        parameters.put("merchant_id", "juspay_recharge");
        final String response = new RestClient().postData("https://api.juspay.in/order/status", parameters);
        JSONObject jsonObject;
        try {
            PaymentResponse paymentResponse = new PaymentResponse();
            jsonObject = new JSONObject(response);
            paymentResponse.setStatus(jsonObject.getString("status"));
            paymentResponse.setOrderId(rechargeRequest.getPaymentId());
            paymentResponse.setMerchantId("juspay_recharge");
            return paymentResponse;
        } catch (JSONException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    protected String serialize(Map<String, ?> parameters) {
        StringBuffer buffer = new StringBuffer();
        for(String key : parameters.keySet()) {
            String value = parameters.get(key).toString();
            buffer.append(key);
            buffer.append("=");
            buffer.append(value == null ? "" : value);
            buffer.append("&");
        }
        Log.d(Constants.TAG, buffer.toString());
        return buffer.toString();
    }
}
