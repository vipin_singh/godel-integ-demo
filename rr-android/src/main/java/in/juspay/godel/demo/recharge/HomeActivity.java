package in.juspay.godel.demo.recharge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import in.juspay.godel.demo.R;

public class HomeActivity extends Activity {

    private Intent intent;
    private Button cardsButton;
    private Button netBankingButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);
        cardsButton = (Button) findViewById(R.id.cards_button);
        netBankingButton = (Button) findViewById(R.id.netbanking_button);
        intent = new Intent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        enableButtons();
    }

    private void enableButtons() {
        cardsButton.setEnabled(true);
        cardsButton.setText("Use Cards");
        netBankingButton.setEnabled(true);
        netBankingButton.setText("Use Netbanking");
    }

    private void disableButtons() {
        cardsButton.setEnabled(false);
        netBankingButton.setEnabled(false);
    }

    public void cardsButtonClicked(View view) {
        disableButtons();
        cardsButton.setText("Processing...");
        intent.setClass(this, RechargeActivity.class);
        startActivity(intent);
    }

    public void netbankingButtonClicked(View view) {
        disableButtons();
        netBankingButton.setText("Processing...");
        intent.setClass(this, NetbankingDemoBrowserActivity.class);
        startActivity(intent);
    }
}
