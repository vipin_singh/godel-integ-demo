package in.juspay.godel.demo.custom;

import android.webkit.WebView;
import in.juspay.godel.browser.JuspayWebViewClient;
import in.juspay.godel.demo.recharge.RechargeActivity;
import in.juspay.godel.ui.JuspayBrowserFragment;
import in.juspay.godel.ui.JuspayWebView;

import java.net.MalformedURLException;
import java.net.URL;

public class CustomWebViewClient extends JuspayWebViewClient {

    private final RechargeActivity rechargeActivity;

    public CustomWebViewClient(JuspayWebView webView, JuspayBrowserFragment browserFragment, RechargeActivity rechargeActivity) {
        super(webView, browserFragment);
        this.rechargeActivity = rechargeActivity;
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        if(view.getTitle() != null && view.getTitle().equals("Juspay Payment Response")) {
            rechargeActivity.handlePaymentResponse(url);
        }
        else {
            super.onPageFinished(view, url);
        }
    }

}
